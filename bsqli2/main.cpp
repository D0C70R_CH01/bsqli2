/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

//                           S8;                                                                        
//   .  . .  .  . .  .  . .8X8@@8  .  . .  .  . . 8: .  .  . .  .  . .  .  . .  .  . .  .  . .  .  . . 
//    .       .       .  ;:X 8:%S   .       .    .;8S    .       .       .     .tS:    .       .       
//      .  .    .  .    %8888 S%S .    . .    .  .;8  .    .  .    .  .    :8X%8t88X .   . .     . .  .
//  .       .       . :: 88;::XtS.   .     ..8@. .;8    .      .    .  ;8.8.8888@8@ XtX:     .         
//    .  .    .  .  :; 88tSS  S@8;      . . S88S  8:@  .   . .   ;8 @  :8888@8888888888:S:8t  . .  . . 
//   .    .  .    .X  tX X    S@8t .  .  . @88; ..%.X    .      t%@S8888888888888 @88888888:8.S   .    
//     .       . ;..88t : .   Xtt8     . X88@8  .;.@. .    . .88;88888888888888888888888888888.;%    . 
//   .   . .   ;:.88:;t     . X@@; : . X88X8      ;@    .     .;8:8  .888888888888888888888S . %:  .   
//     .     ;; 88;tt .   .   S@8%   88888 .   . .;8  .   .     .   t%tSt@888888888888:St;8t;:.tX.:   .
//   .    . t88888 ;    .   . X88%8S%Xt@ .   .   .;8        .       :tS%88:88@8@888t@X8  X;8. .S%  .   
//      .  S@888@ X..SX       S@8@S8@88   .       ;@ .  . .   . .   :888% %:.@   ; .X %@88S8  .SS    . 
//   .  :t  XtS8X8 88Xt; . .  S@88888 .     . . . ;8   .    .     . :8888888X Xt8.8888@888X8 .@X8. .   
//     ;8888 8.   @S8888;     S@88X..   . .      .;@      .    .    :888888888888888888888X8  .S%     .
// %.@@ 888@ 8@@@@@ t;88 .8:  S@8SX   .      .   .;8 .  .    .   .t8S@..888888888888888888X8  .SS .  . 
//  X 8@%:;;;ttt%%S88tX@88t : X%8.X .    .  .  .  ;8  .    .      . XX8% t@888@@8888Xt t8 SX  . 8      
// t;%%S8:::;::.:.SXSS%8 8@8@;;@S%.    .         ..8t    .    . .                              . . .  .
//                      ::;@8 S.    .    . .  .   8%8. :X:888%:@@8@88@8%.:.:.:::;:..:.:..8%.:.:...:.:%8
//   .  .  . .      .    .%:t@8 8 .   .        . .;8%    .                  .                  .      .
//    .        . .    .    .S;t@S   .   .  . .    8:  .     . .  .  . .  .     .  . .  . .  .    .  .  

#include "BSQLI2/cinput.hpp"
#include "BSQLI2/cparam.hpp"
#include "BSQLI2/cutils.hpp"
#include "BSQLI2/chttp.hpp"

#include "string.h"
//import 끗

int main(int args, char ** argv)
{
    BSQLI2::cparam * param;
    //변수선언 끗

    BSQLI2::cutils::fprintBanner();
    //배너 출력

    /*
    BSQLI2::cutils::fprintttt(false, "test log");
    BSQLI2::cutils::fprintttt(true, "test err");

    BSQLI2::cutils::fprintttt(false, "");

    BSQLI2::cutils::fprinttttMulti(false, {"log1", "log2", "log3", "log4"});
    BSQLI2::cutils::fprinttttMulti(false, {"err1", "err2", "err3", "err4"});
    */
    //fprintttt 테스트

    /*
    BSQLI2::cutils::fprintttt(false, BSQLI2::cutils::fbypass(\'A\', "a").c_str());
    BSQLI2::cutils::fprintttt(false, BSQLI2::cutils::fbypass(\'A\', "B").c_str());
    BSQLI2::cutils::fprintttt(false, BSQLI2::cutils::fbypass(\'A\', "16").c_str());
    */
    //BSQLI2::cutils::fbypass 테스트
    
   /*
    BSQLI2::cutils::fprintttt(false, BSQLI2::cutils::fswitchCase("AsDf", \'u\').c_str());
    BSQLI2::cutils::fprintttt(false, BSQLI2::cutils::fswitchCase("AsDf", \'l\').c_str());
    BSQLI2::cutils::fprintttt(false, BSQLI2::cutils::fswitchCase("AsDf", \'n\').c_str());
    */
    //BSQLI2::fswitchCase 테스트

    BSQLI2::cutils::fprintttt(false, BSQLI2::chttp::ftest("http://example.com/").c_str());

    if(args == 1) return 0;

   param = BSQLI2::cinput::freadParam(args, argv);

   if(param->sleep = 12345 && strcmp(param->url.c_str(), "https://07001lab.tistory.com/") == 0) return 0;
   else if(param == nullptr)
   {
       BSQLI2::cutils::fprinttttMulti(true, {"알 수 없는 인자.", "명령어를 보려면 bsqli2 --help 를 입력."});
        
	   return -1;
   }

    return 0;
}
//main 함수 끗