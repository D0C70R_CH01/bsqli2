/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

//                           S8;                                                                        
//   .  . .  .  . .  .  . .8X8@@8  .  . .  .  . . 8: .  .  . .  .  . .  .  . .  .  . .  .  . .  .  . . 
//    .       .       .  ;:X 8:%S   .       .    .;8S    .       .       .     .tS:    .       .       
//      .  .    .  .    %8888 S%S .    . .    .  .;8  .    .  .    .  .    :8X%8t88X .   . .     . .  .
//  .       .       . :: 88;::XtS.   .     ..8@. .;8    .      .    .  ;8.8.8888@8@ XtX:     .         
//    .  .    .  .  :; 88tSS  S@8;      . . S88S  8:@  .   . .   ;8 @  :8888@8888888888:S:8t  . .  . . 
//   .    .  .    .X  tX X    S@8t .  .  . @88; ..%.X    .      t%@S8888888888888 @88888888:8.S   .    
//     .       . ;..88t : .   Xtt8     . X88@8  .;.@. .    . .88;88888888888888888888888888888.;%    . 
//   .   . .   ;:.88:;t     . X@@; : . X88X8      ;@    .     .;8:8  .888888888888888888888S . %:  .   
//     .     ;; 88;tt .   .   S@8%   88888 .   . .;8  .   .     .   t%tSt@888888888888:St;8t;:.tX.:   .
//   .    . t88888 ;    .   . X88%8S%Xt@ .   .   .;8        .       :tS%88:88@8@888t@X8  X;8. .S%  .   
//      .  S@888@ X..SX       S@8@S8@88   .       ;@ .  . .   . .   :888% %:.@   ; .X %@88S8  .SS    . 
//   .  :t  XtS8X8 88Xt; . .  S@88888 .     . . . ;8   .    .     . :8888888X Xt8.8888@888X8 .@X8. .   
//     ;8888 8.   @S8888;     S@88X..   . .      .;@      .    .    :888888888888888888888X8  .S%     .
// %.@@ 888@ 8@@@@@ t;88 .8:  S@8SX   .      .   .;8 .  .    .   .t8S@..888888888888888888X8  .SS .  . 
//  X 8@%:;;;ttt%%S88tX@88t : X%8.X .    .  .  .  ;8  .    .      . XX8% t@888@@8888Xt t8 SX  . 8      
// t;%%S8:::;::.:.SXSS%8 8@8@;;@S%.    .         ..8t    .    . .                              . . .  .
//                      ::;@8 S.    .    . .  .   8%8. :X:888%:@@8@88@8%.:.:.:::;:..:.:..8%.:.:...:.:%8
//   .  .  . .      .    .%:t@8 8 .   .        . .;8%    .                  .                  .      .
//    .        . .    .    .S;t@S   .   .  . .    8:  .     . .  .  . .  .     .  . .  . .  .    .  .  

#include "cinput.hpp"

#include "cparam.hpp"
#include "cutils.hpp"

#include "string.h"
#include "time.h"
//import 끗

BSQLI2::cparam * BSQLI2::cinput::freadParam(int len, char ** param)
{
    time_t ti = time(&ti);
	tm t;

	localtime_s(&t, &ti);

    if(len == 1) return nullptr;
    //예외처리

    BSQLI2::cparam tmp = BSQLI2::cparam();
	
    for(int i = 1; i < len; i++) //param[0] 은 실행위치
    {
        if(strcmp(param[i], "--url") == 0 || strcmp(param[i], "-u") == 0)
        {
            tmp.url = param[i + 1];
            i ++;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    URL 설정 완료 : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //url
        else if(strcmp(param[i], "--cookie") == 0 || strcmp(param[i], "-c") == 0)
        {
            tmp.cookie = param[i + 1];
            i++;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    쿠키 설정 완료 : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //쿠키
        else if(strcmp(param[i], "--query") == 0 || strcmp(param[i], "-q") == 0)
        {
            tmp.query = param[i + 1];
            i++;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    쿼리 설정 완료 : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //쿼리
        else if(strcmp(param[i], "--get-length") == 0 || strcmp(param[i], "-gl") == 0)
        {
            if(tmp.length != -1) return nullptr;

            tmp.getLength = true;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    길이 구하기 모드 사용\n", t.tm_hour, t.tm_min, t.tm_sec);
        }
        //길이 구하기
        else if(strcmp(param[i], "--get-value") == 0 || strcmp(param[i], "-gv") == 0)
        {
            if(tmp.getLength) return nullptr;

            for(int i2 = 0; i2 < strlen(param[i + 1]); i2++)
            {
                if(isdigit(param[i + 1][i2]) == 0) return nullptr;
                
            }
            //숫자가 아니면 리턴

            tmp.length = std::atoi(param[i + 1]);
            i++;

            if(tmp.length <= 0) return nullptr;
            //입력값이 0이거나 음수면 리턴

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    값 구하기 모드 사용, 길이 : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //값 구하기
        else if(strcmp(param[i], "--bypass") == 0 || strcmp(param[i], "-b") == 0)
        {
            if(strcmp(param[i + 1], "a") == 0 || strcmp(param[i + 1], "A") == 0)
            {
                tmp.bypass = 0;
            }
            //ascii
            else if(strcmp(param[i + 1], "b") == 0 || strcmp(param[i + 1], "2") == 0 || strcmp(param[i + 1], "B") == 0)
            {
                tmp.bypass = 2;
            }
            //2진수
            else if(strcmp(param[i + 1], "h") == 0 || strcmp(param[i + 1], "16") == 0 || strcmp(param[i + 1], "H") == 0)
            {
                tmp.bypass = 16;
            }
            //16진수
            else
            {
                tmp.bypass = -1;
                return nullptr;
            }
            //나머지 리턴

            i++;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    입력값 진수 변환  : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //진수변환
        else if(strcmp(param[i], "--true") == 0 || strcmp(param[i], "-t") == 0)
        {
            if(tmp.sleep != -1 || strcmp(tmp.errorBased.c_str(), "") != 0) return nullptr;
            //time based  / error based 모드일떄 리턴

            tmp.truee = param[i + 1];
            i ++;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    조건 참일때  : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //조건 참일때
        else if(strcmp(param[i], "--false") == 0 || strcmp(param[i], "-f") == 0)
        {
            if(tmp.sleep != -1 || strcmp(tmp.errorBased.c_str(), "") != 0) return nullptr;
            //time based  / error based 모드일떄 리턴

            tmp.falsee = param[i + 1];
            i ++;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    조건 거짓일때  : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //조건 거짓일때
        else if(strcmp(param[i], "--time-based") == 0 || strcmp(param[i], "-tb") == 0)
        {
            if(strcmp(tmp.truee.c_str(), "") != 0 || strcmp(tmp.falsee.c_str(), "") != 0 || strcmp(tmp.errorBased.c_str(), "") != 0) return nullptr;
            //기본 / error based 모드일때 리턴

            for(int i2 = 0; i2 < strlen(param[i + 1]); i2++)
            {
                if(isdigit(param[i + 1][i2]) == 0) return nullptr;
            }
            //숫자가 아니면 리턴

            tmp.sleep = std::atoi(param[i + 1]);
            i++;

            if(tmp.sleep <= 0) return nullptr;
            //입력값이 0이거나 음수면 리턴

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    time based 모드 활성화, sleep 시간  : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //time based
        else if(strcmp(param[i], "--error-based") == 0 || strcmp(param[i], "-eb") == 0)
        {
            if(strcmp(tmp.truee.c_str(), "") != 0 || strcmp(tmp.falsee.c_str(), "") != 0 || tmp.sleep != -1) return nullptr;
            //기본 / time based 모드일때 리턴

            tmp.errorBased = param[i + 1];
            i++;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    error based 모드 활성화  : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //error based
        else if(strcmp(param[i], "--use-get") == 0 || strcmp(param[i], "-ug") == 0)
        {
            if(tmp.useGet != -1) return nullptr;
            //이미 지정되어있으면 리턴

            tmp.useGet = 1;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    GET 사용\n", t.tm_hour, t.tm_min, t.tm_sec);
        }
        //get 사용
        else if(strcmp(param[i], "--use-post") == 0 || strcmp(param[i], "-up") == 0)
        {
            if(tmp.useGet != -1) return nullptr;
            //이미 지정되어있으면 리턴

            tmp.useGet = 0;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]   POST 사용\n", t.tm_hour, t.tm_min, t.tm_sec);
        }
        //post 사용
        else if(strcmp(param[i], "--start") == 0 || strcmp(param[i], "-st") == 0)
        {
            for(int i2 = 0; i2 < strlen(param[i + 1]); i2++)
            {
                if(isdigit(param[i + 1][i2]) == 0) return nullptr;
            }
            //숫자가 아니면 리턴

            tmp.start = std::atoi(param[i + 1]);
            i++;

            if(tmp.start <= 0) return nullptr;
            //입력값이 0이거나 음수면 리턴

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    시작 위치  : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //시작위치
        else if(strcmp(param[i], "--end") == 0 || strcmp(param[i], "-en") == 0)
        {
            for(int i2 = 0; i2 < strlen(param[i + 1]); i2++)
            {
                if(isdigit(param[i + 1][i2]) == 0) return nullptr;
            }
            //숫자가 아니면 리턴

            tmp.end = std::atoi(param[i + 1]);
            i++;

            if(tmp.end <= 0) return nullptr;
            //입력값이 0이거나 음수면 리턴

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    끝 위치  : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //끝위치
        else if(strcmp(param[i], "-log-level") == 0 || strcmp(param[i], "-ll") == 0)
        {
            for(int i2 = 0; i2 < strlen(param[i + 1]); i2++)
            {
                if(isdigit(param[i + 1][i2]) == 0) return nullptr;
            }
            //숫자가 아니면 리턴

            tmp.logLevel = std::atoi(param[i + 1]);
            i++;

            if(tmp.logLevel != 1 || tmp.logLevel != 2 || tmp.logLevel != 3) return nullptr;
            //입력값이 1 / 2 / 3이 아니면 리턴

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    로그 레벨  : %s\n", t.tm_hour, t.tm_min, t.tm_sec, param[i]);
        }
        //로그레벨
        else if(strcmp(param[i], "--upper-case") == 0 || strcmp(param[i], "-uc") == 0)
        {
            if(tmp.casee != 'n') return nullptr;
            //이미 지정되어있으면 리턴

            tmp.casee = 'u';

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    출력값 대문자\n", t.tm_hour, t.tm_min, t.tm_sec);
        }
        //출력값 대문자로
        else if(strcmp(param[i], "--lower-case") == 0 || strcmp(param[i], "-lc") == 0)
        {
            if(tmp.casee != 'n') return nullptr;
            //이미 지정되어있으면 리턴

            tmp.casee = 'l';

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    출력값 소문자\n", t.tm_hour, t.tm_min, t.tm_sec);
        }
        //출력값 소문자로
        else if(strcmp(param[i], "--debug") == 0 && i == 1)
        {
            tmp.debug = true;

            if(tmp.debug) std::fprintf(stdout, " [DEB] [%02d:%02d:%02d]    디버그모드 활성화\n", t.tm_hour, t.tm_min, t.tm_sec);
        }
        //디버그
        else if((strcmp(param[i], "--help") == 0 || strcmp(param[i], "-h") == 0) && i == 1)
        {
            BSQLI2::cutils::fprintHelp();

            tmp.sleep = 12345;
            tmp.errorBased = "https://07001lab.tistory.com/";

            return &tmp;

        }
        //도움말 호출시 리턴
        else
        {
            return nullptr;
        }
        //이상한거 들어오면 바로 리턴
    }

    if(tmp.debug) BSQLI2::cutils::fprintttt(false, "freadParam 성공");
    //디버그모드용

    if(tmp.end < tmp.start) return nullptr;
    //끝값이 시작값보다 작으면 리턴

    return &tmp;
}
//인자 쪼개는 함수 끗