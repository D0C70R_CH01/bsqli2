/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

//                           S8;                                                                        
//   .  . .  .  . .  .  . .8X8@@8  .  . .  .  . . 8: .  .  . .  .  . .  .  . .  .  . .  .  . .  .  . . 
//    .       .       .  ;:X 8:%S   .       .    .;8S    .       .       .     .tS:    .       .       
//      .  .    .  .    %8888 S%S .    . .    .  .;8  .    .  .    .  .    :8X%8t88X .   . .     . .  .
//  .       .       . :: 88;::XtS.   .     ..8@. .;8    .      .    .  ;8.8.8888@8@ XtX:     .         
//    .  .    .  .  :; 88tSS  S@8;      . . S88S  8:@  .   . .   ;8 @  :8888@8888888888:S:8t  . .  . . 
//   .    .  .    .X  tX X    S@8t .  .  . @88; ..%.X    .      t%@S8888888888888 @88888888:8.S   .    
//     .       . ;..88t : .   Xtt8     . X88@8  .;.@. .    . .88;88888888888888888888888888888.;%    . 
//   .   . .   ;:.88:;t     . X@@; : . X88X8      ;@    .     .;8:8  .888888888888888888888S . %:  .   
//     .     ;; 88;tt .   .   S@8%   88888 .   . .;8  .   .     .   t%tSt@888888888888:St;8t;:.tX.:   .
//   .    . t88888 ;    .   . X88%8S%Xt@ .   .   .;8        .       :tS%88:88@8@888t@X8  X;8. .S%  .   
//      .  S@888@ X..SX       S@8@S8@88   .       ;@ .  . .   . .   :888% %:.@   ; .X %@88S8  .SS    . 
//   .  :t  XtS8X8 88Xt; . .  S@88888 .     . . . ;8   .    .     . :8888888X Xt8.8888@888X8 .@X8. .   
//     ;8888 8.   @S8888;     S@88X..   . .      .;@      .    .    :888888888888888888888X8  .S%     .
// %.@@ 888@ 8@@@@@ t;88 .8:  S@8SX   .      .   .;8 .  .    .   .t8S@..888888888888888888X8  .SS .  . 
//  X 8@%:;;;ttt%%S88tX@88t : X%8.X .    .  .  .  ;8  .    .      . XX8% t@888@@8888Xt t8 SX  . 8      
// t;%%S8:::;::.:.SXSS%8 8@8@;;@S%.    .         ..8t    .    . .                              . . .  .
//                      ::;@8 S.    .    . .  .   8%8. :X:888%:@@8@88@8%.:.:.:::;:..:.:..8%.:.:...:.:%8
//   .  .  . .      .    .%:t@8 8 .   .        . .;8%    .                  .                  .      .
//    .        . .    .    .S;t@S   .   .  . .    8:  .     . .  .  . .  .     .  . .  . .  .    .  .  

#ifndef ___BSQLI2_CPARAM_HPP___
#define ___BSQLI2_CPARAM_HPP___

#include "string"

namespace BSQLI2
{
    class cparam
    {
        public:
            std::string url;
            //URL

            int start;
            int end;
            //start : 시작값
            //end : 끝값

            bool getLength;

            int length;
            //입력받은 길이

            std::string cookie;
            //쿠키

            std::string query;
            //쿼리

            std::string truee;
            std::string falsee;
            //truee : 구문이 true 일때 출력되는 값
            //falsee : 구문이 false 일때 출력되는 값

            std::string errorBased;
            //error based 전용

            int sleep;
            //time based 전용

            char useGet;
            //1 = GET 0 = POST -1 = 에러

            char logLevel;
            //1 = true 인경우에만 출력 2 = 이상한 출력이 나온 경우도 출력 3 = 전부 출력

            char bypass;
            //0 = ASCII 2 = 2진수 16 = 16진수 나머지 = 에러

            char casee;
            //n = 변경안함 u = 대문자 l = 소문자

            bool debug;
            
            //변수선언 끗

            cparam();
            //생성자 끗
    };
    //클래스 선언 끗
}
//네임스페이스 선언 끗

#endif