/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

//                           S8;                                                                        
//   .  . .  .  . .  .  . .8X8@@8  .  . .  .  . . 8: .  .  . .  .  . .  .  . .  .  . .  .  . .  .  . . 
//    .       .       .  ;:X 8:%S   .       .    .;8S    .       .       .     .tS:    .       .       
//      .  .    .  .    %8888 S%S .    . .    .  .;8  .    .  .    .  .    :8X%8t88X .   . .     . .  .
//  .       .       . :: 88;::XtS.   .     ..8@. .;8    .      .    .  ;8.8.8888@8@ XtX:     .         
//    .  .    .  .  :; 88tSS  S@8;      . . S88S  8:@  .   . .   ;8 @  :8888@8888888888:S:8t  . .  . . 
//   .    .  .    .X  tX X    S@8t .  .  . @88; ..%.X    .      t%@S8888888888888 @88888888:8.S   .    
//     .       . ;..88t : .   Xtt8     . X88@8  .;.@. .    . .88;88888888888888888888888888888.;%    . 
//   .   . .   ;:.88:;t     . X@@; : . X88X8      ;@    .     .;8:8  .888888888888888888888S . %:  .   
//     .     ;; 88;tt .   .   S@8%   88888 .   . .;8  .   .     .   t%tSt@888888888888:St;8t;:.tX.:   .
//   .    . t88888 ;    .   . X88%8S%Xt@ .   .   .;8        .       :tS%88:88@8@888t@X8  X;8. .S%  .   
//      .  S@888@ X..SX       S@8@S8@88   .       ;@ .  . .   . .   :888% %:.@   ; .X %@88S8  .SS    . 
//   .  :t  XtS8X8 88Xt; . .  S@88888 .     . . . ;8   .    .     . :8888888X Xt8.8888@888X8 .@X8. .   
//     ;8888 8.   @S8888;     S@88X..   . .      .;@      .    .    :888888888888888888888X8  .S%     .
// %.@@ 888@ 8@@@@@ t;88 .8:  S@8SX   .      .   .;8 .  .    .   .t8S@..888888888888888888X8  .SS .  . 
//  X 8@%:;;;ttt%%S88tX@88t : X%8.X .    .  .  .  ;8  .    .      . XX8% t@888@@8888Xt t8 SX  . 8      
// t;%%S8:::;::.:.SXSS%8 8@8@;;@S%.    .         ..8t    .    . .                              . . .  .
//                      ::;@8 S.    .    . .  .   8%8. :X:888%:@@8@88@8%.:.:.:::;:..:.:..8%.:.:...:.:%8
//   .  .  . .      .    .%:t@8 8 .   .        . .;8%    .                  .                  .      .
//    .        . .    .    .S;t@S   .   .  . .    8:  .     . .  .  . .  .     .  . .  . .  .    .  .  

#pragma warning(disable : 4996)

#include "cutils.hpp"

#include "iostream"
#include "time.h"
#include "algorithm"
#include "cctype"
//import 끗

void BSQLI2::cutils::fprintBanner()
{
    std::cout << " ____   _____  ____  _      _____ ___  \n";
    std::cout << "|  _ \\ / ____|/ __ \\| |    |_   _|__ \\ \n";
    std::cout << "| |_) | (___ | |  | | |      | |    ) |\n";
    std::cout << "|  _ < \\___ \\| |  | | |      | |   / / \n";
    std::cout << "| |_) |____) | |__| | |____ _| |_ / /_ \n";
    std::cout << "|____/|_____/ \\___\\_\\______|_____|____|\n";
    std::cout << std::endl;
    std::cout << "Copyright (c) 2020 최박사 | D0C70R_CH01\n";
	std::cout << std::endl;
};
//배너 출력 함수 끗

unsigned int BSQLI2::cutils::fnow()
{
	clock_t t = clock();

    return (unsigned int) t;
}
//실행하고부터 지금까지의 시간 출력하는 함수 끗

unsigned int BSQLI2::cutils::fnowSec()
{
    time_t t = time(&t);

    return (unsigned int) t;
}
//지금 시간 출력하는 함수 끗

void BSQLI2::cutils::fprintttt(bool error, const char * data)
{
    time_t ti = time(&ti);
	tm t;

	localtime_s(&t, &ti);

    if(error)
    {
        std::fprintf(stderr, " [ERR] [%02d:%02d:%02d]    %s\n", t.tm_hour, t.tm_min, t.tm_sec, data);
    }
    else
    {
        std::fprintf(stdout, " [LOG] [%02d:%02d:%02d]    %s\n", t.tm_hour, t.tm_min, t.tm_sec, data);
    }
}
//무언가를 출력하는 함수 끗

void BSQLI2::cutils::fprinttttMulti(bool error, std::initializer_list<const char *> data)
{
    for(const char * c : data)
    {
        BSQLI2::cutils::fprintttt(error, c);
    }
}
//여러줄의 무언가를 출력하는 함수 끗

std::string BSQLI2::cutils::fbypass(int i, const char * c)
{
    std::string ret;

    if(c == "a" || c== "A")
    {
        ret = (char) i;
    }
    else if(c == "b" || c == "B" || c == "2")
    {
        while(i > 0)
        {
            if(i %2 == 1)
            {
                ret += '1';
            }
            else
            {
                ret += '0';
            }
            i /= 2;
        }
        
        std::reverse(ret.begin(), ret.end());
    }
    else if(c == "h" || c == "H" || c == "16")
    {
        char buf[2];

        std::sprintf(buf, "%x", i);

        ret = std::string(buf);
    }
    else
    {
        return "!!!! error !!!!";
    }
    
    return ret;
}
//진수변환 하는 함수 끗

std::string BSQLI2::cutils::fswitchCase(std::string data, char c)
{
    if(c == 'u')
    {
        std::transform(data.begin(), data.end(), data.begin(), toupper);
        return data;
    }
    else if(c == 'l')
    {
        std::transform(data.begin(), data.end(), data.begin(), tolower);
        return data;
    }
    else
    {
        return data;
    }
    
}
//출력값 대소문자 바꾸는 함수 끗

void BSQLI2::cutils::fprintHelp()
{
    BSQLI2::cutils::fprinttttMulti(false, {"사용법 : bsqli2 [param]",
                                                        "--help -h",
                                                        "  도움말을 출력함",
                                                        "--url -u [value]",
                                                        "  타겟 URL을 입력함",
                                                        "--cookie -c [value]",
                                                        "  추가로 전송해줘야할 쿠키를 입력함",
                                                        "--query -q [value]",
                                                        "  공격에 사용할 쿼리.",
                                                        "  length(col)={@!LENGTH}",
                                                        "  substr(col,{@!INDEX},1)=\'{@!VALUE}\'",
                                                        "  식으로 입력",
                                                        "--get-length -gl",
                                                        "  길이를 구하는 쿼리를 날림.",
                                                        "  {@!LENGTH} 를 찾아서 가변시킴.",
                                                        "  length(col)={@!LENGTH} -> length(col)=0 -> length(col)=1 . . .",
                                                        "  밑에거랑 같이쓰면 오류띄움",
                                                        "--get-value -gv [value]",
                                                        "  총 글자수를 입력받음",
                                                        "  값을 구하는 쿼리를 날림.",
                                                        "  {@!INDEX} 와 {@!VALUE} 를 찾아서 가변시킴.",
                                                        "  substr(col. {@!INDEX},1)=\'{@!VALUE}\' -> substr(col,1,1)=\'a\' . . .",
                                                        "  위에거랑 같이쓰면 오류띄움",
                                                        "--bypass -b [a:A:b:B:2:h:H:16]",
                                                        "  {@!VALUE} 를 변환함.",
                                                        "  자동으로 0x 나 0b 를 달아주지는 않음",
                                                        "  당연히 \'\' 도 달아주지 않음.",
                                                        "  a A : ASCII / a -> a",
                                                        "  b B 2 : 2진수 / a -> 01100001",
                                                        "  h H 16 : 16진수 / a -> 61",
                                                        "--true -t [value]",
                                                        "  값이 참일때 출력되는 값을 입력받음.",
                                                        "--false -f [value]",
                                                        "  값이 거짓일때 출력되는 값을 입력받음.",
                                                        "--time-based -tb [value]",
                                                        "  time based 할때 지연되는 초를 입력받음.",
                                                        "--error-based -eb [value]",
                                                        "  error based용.",
                                                        "  넣은 값을 찾을수 없으면 OK 찾을수 있으면 FAIL",
                                                        "--use-get -ug",
                                                        "  get 사용.",
                                                        "  밑에거랑 같이쓰면 오류띄움",
                                                        "--use-post -up",
                                                        "  post 사용.",
                                                        "  위에거랑 같이쓰면 오류띄움",
                                                        "--start -st [value (default:0)]",
                                                        "  길이 찾기 / 값 찾기 시작값을 입력함.",
                                                        "--end -en [value (default: 100)]",
                                                        "  길이 찾기 끝값을 입력함.",
                                                        "--log-level -ll [1:2:3 (default:2)]",
                                                        "  1 : OK 만 출력",
                                                        "  2 : OK / ???? 출력",
                                                        "  3 : 전부 출력",
                                                        "--upper-case -uc",
                                                        "  출력값을 대문자로 바꿈",
                                                        "  밑에거랑 같이쓰면 오류남",
                                                        "--lower-case -lc",
                                                        "  출력값을 소문자로 바꿈",
                                                        "  위에거랑 같이쓰면 오류남",
                                                        "--debug",
                                                        "  디버그 전용",
                                                        "  무조건 첫번째 인자일때만 작동함"});
}
//도움말 출력하는 함수 끗