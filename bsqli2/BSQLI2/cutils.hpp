/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

//                           S8;                                                                        
//   .  . .  .  . .  .  . .8X8@@8  .  . .  .  . . 8: .  .  . .  .  . .  .  . .  .  . .  .  . .  .  . . 
//    .       .       .  ;:X 8:%S   .       .    .;8S    .       .       .     .tS:    .       .       
//      .  .    .  .    %8888 S%S .    . .    .  .;8  .    .  .    .  .    :8X%8t88X .   . .     . .  .
//  .       .       . :: 88;::XtS.   .     ..8@. .;8    .      .    .  ;8.8.8888@8@ XtX:     .         
//    .  .    .  .  :; 88tSS  S@8;      . . S88S  8:@  .   . .   ;8 @  :8888@8888888888:S:8t  . .  . . 
//   .    .  .    .X  tX X    S@8t .  .  . @88; ..%.X    .      t%@S8888888888888 @88888888:8.S   .    
//     .       . ;..88t : .   Xtt8     . X88@8  .;.@. .    . .88;88888888888888888888888888888.;%    . 
//   .   . .   ;:.88:;t     . X@@; : . X88X8      ;@    .     .;8:8  .888888888888888888888S . %:  .   
//     .     ;; 88;tt .   .   S@8%   88888 .   . .;8  .   .     .   t%tSt@888888888888:St;8t;:.tX.:   .
//   .    . t88888 ;    .   . X88%8S%Xt@ .   .   .;8        .       :tS%88:88@8@888t@X8  X;8. .S%  .   
//      .  S@888@ X..SX       S@8@S8@88   .       ;@ .  . .   . .   :888% %:.@   ; .X %@88S8  .SS    . 
//   .  :t  XtS8X8 88Xt; . .  S@88888 .     . . . ;8   .    .     . :8888888X Xt8.8888@888X8 .@X8. .   
//     ;8888 8.   @S8888;     S@88X..   . .      .;@      .    .    :888888888888888888888X8  .S%     .
// %.@@ 888@ 8@@@@@ t;88 .8:  S@8SX   .      .   .;8 .  .    .   .t8S@..888888888888888888X8  .SS .  . 
//  X 8@%:;;;ttt%%S88tX@88t : X%8.X .    .  .  .  ;8  .    .      . XX8% t@888@@8888Xt t8 SX  . 8      
// t;%%S8:::;::.:.SXSS%8 8@8@;;@S%.    .         ..8t    .    . .                              . . .  .
//                      ::;@8 S.    .    . .  .   8%8. :X:888%:@@8@88@8%.:.:.:::;:..:.:..8%.:.:...:.:%8
//   .  .  . .      .    .%:t@8 8 .   .        . .;8%    .                  .                  .      .
//    .        . .    .    .S;t@S   .   .  . .    8:  .     . .  .  . .  .     .  . .  . .  .    .  .  

#ifndef ___BSQLI2_CUTILS_HPP___
#define ___BSQLI2_CUTILS_HPP___

#include "string"

namespace BSQLI2
{
    namespace cutils
    {
        void fprintBanner();
        //배너 출력 함수 끗

        unsigned int fnow();
        //실행하고부터 지금까지의 시간 출력하는 함수 끗

        unsigned int fnowSec();
        //지금 시간 출력하는 함수 끗

        void fprintttt(bool error, const char * data);
        //무언가를 출력하는 함수 끗
        
        void fprinttttMulti(bool error, std::initializer_list<const char *> data);
        //여러줄의 무언가를 출력하는 함수 끗

        std::string fbypass(int i, const char * c);
        //진수변환 하는 함수 끗

        std::string fswitchCase(std::string data, char c);
        //출력값 대소문자 바꾸는 함수 끗

        void fprintHelp();
        //도움말 출력하는 함수 끗
    }
}

#endif