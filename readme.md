# BSQLI2  

[![DoctorChoi](./4lab_DoctorChoi.png "Made by 4lab_DoctorChoi")](https://07001lab.tistory.com/)

> 난 그저 webhacking.kr 를 편하게 풀고 싶었을 뿐임  
> -최박사-  

> SQL 만큼 더러운 문법도 없다.  
> -익명-  

## 패치노트  

[about:blank](about:blank)

## 사용법  

```bash
bsqli2 --url [value] --cookie [value] --query [value] ((--true [value] & --false [value]) | --sleep [value] | --error-based [value]) (--get-length | --get-value [value]) (--use-get | use-post) ?(--start [value] | --end [value] | --bypass [value] | --log-level [value] | (--upper-case | --lower-case))
```

### --help -h  

도움말을 출력함  

### --url -u [value]

타겟 URL을 입력함  

### --cookie -c [value]

추가로 전송해줘야할 쿠키를 입력함  

### --query -q [value]

공격에 사용할 쿼리.  
length(col)={@!LENGTH}  
substr(col,{@!INDEX},1)='{@!VALUE}'  
식으로 입력  

### --get-length -gl  

길이를 구하는 쿼리를 날림.  
{@!LENGTH} 를 찾아서 가변시킴.  
length(col)={@!LENGTH} -> length(col)=0 -> length(col)=1 . . .  
밑에거랑 같이쓰면 오류띄움  

### --get-value -gv [value]  

총 글자수를 입력받음  
값을 구하는 쿼리를 날림.  
{@!INDEX} 와 {@!VALUE} 를 찾아서 가변시킴.  
substr(col. {@!INDEX},1)='{@!VALUE}' -> substr(col,1,1)='a' -> substr(col,2,1)='b' . . .  
위에거랑 같이쓰면 오류띄움  

### --bypass -b [a:A:b:B:2:h:H:16]

{@!VALUE} 를 변환함.
자동으로 0x 나 0b 를 달아주지는 않음  
당연히 '' 도 달아주지 않음.  
a A : ASCII / a -> a  
b B 2 : 2진수 / a -> 01100001  
h H 16 : 16진수 / a -> 61  

### --true -t [value]

값이 참일때 출력되는 값을 입력받음.

### --false -f [value]

값이 거짓일때 출력되는 값을 입력받음.

### --time-based -tb [value]

time based 할때 지연되는 초를 입력받음.  

### --error-based -eb [value]

error based용.  
넣은 값을 찾을수 없으면 OK 찾을수 있으면 FAIL

### --use-get -ug

get 사용.  
밑에거랑 같이쓰면 오류띄움  

### --use-post -up

post 사용.  
위에거랑 같이쓰면 오류띄움  

### --start -st [value (default:0)]

길이 찾기 / 값 찾기 시작값을 입력함.  

### --end -en [value (default: 100)]  

길이 찾기 끝값을 입력함.  

### --log-level -ll [1:2:3 (default:2)]

1 : OK 만 출력  
2 : OK / ???? 출력  
3 : 전부 출력

### --upper-case -uc  

출력값을 대문자로 바꿈  
밑에거랑 같이쓰면 오류남  

### --lower-case -lc  

출력값을 소문자로 바꿈  
위에거랑 같이쓰면 오류남

### --debug

디버그 전용  
무조건 첫번째 인자일때만 작동함  
